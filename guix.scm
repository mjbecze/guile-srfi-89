(use-modules (gnu packages autotools)
             (gnu packages guile)
             (gnu packages pkg-config)
             (guix git-download)
             (guix build-system guile)
	     (guix gexp)
             ((guix licenses))
             (guix packages))

(define this-directory
  (dirname (current-filename)))

(define source
  (local-file this-directory
	      #:recursive? #t
	      #:select? (git-predicate this-directory)))

(package
    (name "guile-srfi-89")
    (version "0.0.1")
    (source source)
    (build-system guile-build-system)
    (arguments
     '(#:modules ((guix build guile-build-system)
		  (guix build utils)
		  (srfi srfi-64))
       #:phases
       (modify-phases %standard-phases
         (add-after 'build 'check
           (lambda _
	     (let ((load-path (getcwd)))
		(add-to-load-path load-path)
		(load (string-append load-path "/tests/tests.scm"))
		(zero? (test-runner-fail-count (test-runner-current)))))))))
    (native-inputs
     `(("guile" ,guile-3.0)))
    (home-page "https://gitlab.com/mjbecze/guile-srfi-89")
    (synopsis "A hygienic implementation of srfi-89 for guile")
    (description
     "This package provides srfi-89 optional positional and named
parameters, which  define* and lambda* special forms")
    (license gpl3+))
