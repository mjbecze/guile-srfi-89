# GUILE SRFI 89

This is a hygienic implemention of [SRFI 89](https://srfi.schemers.org/srfi-89/srfi-89.html) (Optional positional and named parameters) for Guile Scheme. Implemention details can be found [here](http://nullradix.eth.link/srfi-89-support-of-guile.html).

# INSTALL

`guix install guile-srfi-89`

# TEST

`guix build -f ./guix.scm`

# LICENSE

GPL-3
